To edit CSS, clone the repo and edit it:

0. ``git clone <link at the top right>``
1. ``git pull`` to grab latest file
2. EDIT files
3. ``git add . ; git  commit -m "Update CSS" ; git push origin master``
    ^ macro of 3 commands, `git add .` adds all file changes to project, then you commit and push it
4. Wait for the runner to complete the site hosting (10-20 seconds usually), project marker becomes green when it's done (top right)